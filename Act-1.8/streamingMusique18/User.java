package streamingMusique18;

import java.util.ArrayList;
import java.util.Collections;

import streamingMusique18.MusiqueImpl;
import streamingMusique18.PlayListeImpl;

public class User {

	private String nomUser;
	private String prenomUser;
	private String pseudoUser;
	private ArrayList<PlayListeImpl> playlistes = new ArrayList<PlayListeImpl>();

	// constructeur avec paramètres
	public User(String nomUser, String prenomUser, String pseudoUser) {
		this.nomUser = nomUser;
		this.prenomUser = prenomUser;
		this.pseudoUser = pseudoUser;
		this.playlistes = new ArrayList<PlayListeImpl>();
	}

	// getters and setters
	public String getNomUser() {
		return nomUser;
	}

	public void setNomUser(String nomUser) {
		this.nomUser = nomUser;
	}

	public String getPrenomUser() {
		return prenomUser;
	}

	public void setPrenomUser(String prenomUser) {
		this.prenomUser = prenomUser;
	}

	public String getPseudoUser() {
		return pseudoUser;
	}

	public void setPseudoUser(String pseudoUser) {
		this.pseudoUser = pseudoUser;
	}

	public ArrayList<PlayListeImpl> getPlaylistes() {
		return playlistes;
	}

	public void setPlaylistes(ArrayList<PlayListeImpl> playlistes) {
		this.playlistes = playlistes;
	}

	// method toString
	public String toString() {
		String str = "L'utilisateur s'appelle " + this.prenomUser + " "
				+ this.nomUser + " . Son pseudo est: " + this.pseudoUser
				+ ".\n";
		for (PlayListeImpl playliste : playlistes) {
			str = str + playliste;
		}
		return str;
	}

	// method add playlist
	public void ajouterPlaylist(PlayListeImpl playliste) {
		this.playlistes.add(playliste);
	}

	// search in a playlist for duplicate music
	public boolean contains(String titre, String auteur) {
		for (PlayListeImpl playliste : playlistes) {
			for (MusiqueImpl musique : playliste.getListeMusique()) {
				if (musique.getTitre().equals(titre)
						&& musique.getAutheur().equals(auteur)) {
					return true;
				}
			}
		}
		return false;
	}

	// sort the music list in ascending order
	public void triAsc() {
		for (PlayListeImpl playliste : playlistes) {
			Collections.sort(playliste.getListeMusique());
		}
	}

	// sort the music list in descending order
	public void triDesc() {
		for (PlayListeImpl playliste : playlistes) {
			Collections.sort(playliste.getListeMusique(),
					Collections.reverseOrder());
		}
	}

	// method delete music by title
	public void remove(String titre) {
		for (PlayListeImpl playliste : playlistes) {
			playliste.remove(titre);
		}
	}

}
