package streamingMusique18;

import streamingMusique18.MusiqueImpl;

public interface PlayListe {

	public String toString();

	public void add(MusiqueImpl musique);

	public void removeDuplicate();

}
