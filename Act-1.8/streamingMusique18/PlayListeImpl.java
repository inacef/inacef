package streamingMusique18;

import java.util.ArrayList;

import streamingMusique18.MusiqueImpl;
import streamingMusique18.PlayListe;

//import java.util.Collections;

public class PlayListeImpl implements PlayListe {

	private String nom;
	private String genre;
	private int nbTitres;
	private ArrayList<MusiqueImpl> listeMusique = new ArrayList<>();

	// constructeur avec param�tres
	public PlayListeImpl(String nom, String genre) {
		this.nom = nom;
		this.genre = genre;
		this.nbTitres = listeMusique.size();
		this.listeMusique = new ArrayList<MusiqueImpl>();
	}

	// cr�ation des getters
	public String getNom() {
		return nom;
	}

	public String getGenre() {
		return genre;
	}

	public int getNbTitres() {
		return nbTitres;
	}

	public ArrayList<MusiqueImpl> getListeMusique() {
		return listeMusique;
	}

	// cr�ation des setters
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public void setNbTitres(int nbTitres) {
		this.nbTitres = nbTitres;
	}

	public void setPlayliste(ArrayList<MusiqueImpl> playliste) {
		this.listeMusique = playliste;
	}

	public String toString() {

		String playlisteString = "La playliste s'appelle " + this.getNom()
				+ ".\nElle contient " + this.getNbTitres()
				+ " chansons de genre " + this.getGenre()
				+ ".\nElle contient les titres suivants: \n";

		for (int j = 0; j < listeMusique.size(); j++) {
			playlisteString = playlisteString + listeMusique.get(j).toString()
					+ "\n";
		}
		return playlisteString;
	}

	public void add(MusiqueImpl musique) {
		listeMusique.add(musique);
		nbTitres++;
	}

	public void removeDuplicate() {
		for (int i = 0; i < listeMusique.size(); i++) {
			for (int j = i + 1; j < listeMusique.size(); j++) {
				if (listeMusique.get(i).equals(listeMusique.get(j)) == true) {
					listeMusique.remove(i);
					nbTitres--;
				}
			}

		}
	}

	public void remove(String titre) {
		for (int i = 0; i < listeMusique.size(); i++) {
			if (listeMusique.get(i).getTitre().equals(titre)) {
				listeMusique.remove(i);
				nbTitres--;
			}
		}
	}
}
