package streamingMusique18;

import streamingMusique18.MusiqueImpl;

public interface Musique {

	public String toString();

	public boolean equals(MusiqueImpl musique);

}
