package sauvegardeFichier18;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class LireFichier {

	public static void main(String[] args)throws FileNotFoundException {
		String nomFichier = "Act-1.8/sauvegardeFichier/fichierMusique.txt";
		File file = new File(nomFichier);
		Scanner in = new Scanner(file);
		while (in.hasNextLine()){
			String line = in.nextLine();
			System.out.println(line);
		}
		in.close();
	}	

}
