#Author: ines.nacef1990@gmail.com

Feature: Jumia Ordering
  As a customor, I want to buy items from Jumia so that I accessed to Jumia application and 
  check items to buy them.

  Scenario: Choosing Jumia Items
    Given I am on Jumia poduct page
    When I choose and add Television
    And I choose and add Air conditioner
    And I choose and add Washing machine
    Then I finalize the order
    And I check the products chosen with the right labels and the right prices and the total correctly calculated