package StepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class Steps {
	
	WebDriver myDriver = new ChromeDriver();
	
	@Given("I am on Jumia poduct page")
	public void I_am_on_Jumia_poduct_page()throws Throwable{
		System.setProperty("webdriver.chrome.driver", "C:/Users/Lenovo/Downloads/chromedriver_win32/chromedriver.exe");
		//myDriver.get("https://www.jumia.com.tn");
		myDriver.navigate().to("https://www.jumia.com.tn");
	}
	
    @When("I choose and add Television")
    public void I_choose_and_add_Television()throws Throwable{
    	WebElement keyword = myDriver.findElement(By.name("q"));
        // Enter something to search for
        keyword.sendKeys("T�l�vision");
        // Now submit the form. WebDriver will find the form for us from the element
        keyword.submit();
        Thread.sleep(3000);
        // Choose an item
        WebElement item = myDriver.findElement(By.xpath("/html/body/main/section/section[2]/div[1]/a"));
    	item.click();
    	Thread.sleep(3000);
    	// Click on add to the basket
    	WebElement add = myDriver.findElement(By.className("btn_prim_i-fw"));
    	add.click();
    	Thread.sleep(3000);
    	// Click on continue my purchases
    	WebElement continuePurchases = myDriver.findElement(By.className("btn_sec-mrs-fw"));
    	continuePurchases.click();
    	Thread.sleep(3000);
    }
     
    @When("I choose and add Air conditioner")
    public void I_choose_and_add_Air_conditioner()throws Throwable{
    	WebElement keyword = myDriver.findElement(By.name("q"));
        // Enter something to search for
        keyword.sendKeys("Climatiseur");
        // Now submit the form. WebDriver will find the form for us from the element
        keyword.submit();
        Thread.sleep(3000);
        // Choose an item
        WebElement item = myDriver.findElement(By.xpath("/html/body/main/section/section[2]/div[1]/a"));
    	item.click();
    	Thread.sleep(3000);
    	// Click on add to the basket
    	WebElement add = myDriver.findElement(By.className("btn_prim_i-fw"));
    	add.click();
    	Thread.sleep(3000);
    	// Click on continue my purchases
    	WebElement continuePurchases = myDriver.findElement(By.className("btn_sec-mrs-fw"));
    	continuePurchases.click();
    	Thread.sleep(3000);
    }
    
    @When("I choose and add Washing machine")
    public void I_choose_and_add_Washing_machine()throws Throwable{
    	WebElement keyword = myDriver.findElement(By.name("q"));
        // Enter something to search for
        keyword.sendKeys("Machine � laver");
        // Now submit the form. WebDriver will find the form for us from the element
        keyword.submit();
        Thread.sleep(3000);
        // Choose an item
        WebElement item = myDriver.findElement(By.xpath("/html/body/main/section/section[2]/div[1]/a"));
    	item.click();
    	Thread.sleep(3000);
    	// Click on add to the basket
    	WebElement add = myDriver.findElement(By.className("btn_prim_i-fw"));
    	add.click();
    	Thread.sleep(3000);
;
    }
    
    @Then("I finalize the order")
    public void I_finalize_the_order()throws Throwable{
    	WebElement order = myDriver.findElement(By.className("btn_prim-mls-fw"));
    	order.click();
    	Thread.sleep(3000);
    }
    
    @Then("I check the products chosen with the right labels, the right prices and the total correctly calculated")
    public void I_check_the_products_chosen_with_the_right_labels_and_the_right_prices_and_the_total_correctly_calculated()throws Throwable{
    	
    	//checking Televison right label and right price
    	String RightLabelTV = "T�l�viseur LED - 24'HD- 1824022 - Garantie 3 Ans";
    	String RightPriceTV = "248";
    	
    	String labelTV = myDriver.findElement(By.xpath("/html/body/div[4]/div[2]/form[3]/div/div[2]/a")).getText();
    	if (labelTV.contentEquals(RightLabelTV)){
    		System.out.println("Yes! the right label for Television");
    	}else{
    		System.out.println("No! it's not the right label for Television");
    	}
    	String priceTV = myDriver.findElement(By.xpath("/html/body/div[4]/div[2]/form[3]/div/div[5]/span[1]")).getAttribute("data-price");
    	if (priceTV.contentEquals (RightPriceTV)){
    		System.out.println("Yes! the rignt price for Television");
    	}else{
    		System.out.println("No! it's not the right price for Television");
    	}
    	
    	//checking Air conditioner right label and right price
    	String RightLabelClim = "Climatiseur GENERAL - 12000 BTU - Chaud / Froid - Garantie 1 An";
    	String RightPriceClim = "1197";
    	
    	String labelClim = myDriver.findElement(By.xpath("/html/body/div[4]/div[2]/form[2]/div/div[2]/a")).getText();
    	if (labelClim.contentEquals(RightLabelClim)){
    		System.out.println("Yes! the right label for Air conditioner");
    	}else{
    		System.out.println("No! it's not the right label for Air conditioner");
    	}
    	String priceClim = myDriver.findElement(By.xpath("/html/body/div[4]/div[2]/form[2]/div/div[5]/span[1]")).getAttribute("data-price");
    	if (priceClim.contentEquals (RightPriceClim)){
    		System.out.println("Yes! the rignt price for Air conditioner");
    	}else{
    		System.out.println("No! it's not the right price for Air conditioner");
    	}
    	
    	//checking washing machine right label and right price
    	String RightLabelVM = "Machine � Laver Automatique - 5 KG - Blanc - Garantie 2 Ans";
    	String RightPriceVM = "849";
    	
    	String labelVM = myDriver.findElement(By.xpath("/html/body/div[4]/div[2]/form[1]/div/div[2]/a")).getText();
    	if (labelVM.contentEquals(RightLabelVM)){
    		System.out.println("Yes! the right label for Air conditioner");
    	}else{
    		System.out.println("No! it's not the right label for Air conditioner");
    	}
    	String priceVM = myDriver.findElement(By.xpath("/html/body/div[4]/div[2]/form[1]/div/div[5]/span[1]")).getAttribute("data-price");
    	if (priceVM.contentEquals (RightPriceVM)){
    		System.out.println("Yes! the rignt price for Air conditioner");
    	}else{
    		System.out.println("No! it's not the right price for Air conditioner");
    	}
    	
    	// Checking Total
    	String Total = "2294";
    	
    	String TotalVerif = myDriver.findElement(By.className("row total ft-total")).getAttribute("data-price");
    	if (TotalVerif.contentEquals(Total)){
    		System.out.println("Yes! the total was correctly calculated");
    	}else{
    		System.out.println("No! the total wasn't correctly calculated");
    	}
    	
    }

}
