﻿
public class Bateau {

    protected int id;
    public String nom;
    private String marque;
    public Coque coque;
    public Cabine cabine;
    public Pont pont;
	
	/*constructeur par défaut*/	
    
    public Bateau(){
    	
    }
    
    /*constructeur avec paramétres*/
    
    public Bateau(int id, String nom, String marque, Coque coque, Cabine cabine, Pont pont){
    	this.id = id;
    	this.nom = nom;
    	this.marque = marque;
        this.coque = coque;
        this.cabine = cabine;
        this.pont = pont;
    }
    
    /*création des getters*/
    
    public int getId(){
		return id;
    }
    
    public String getNom(){
		return nom;
    }
    
    public String getMarque(){
		return marque;
    }
    
    public Coque getCoque(){
        return coque;
    }

    public Cabine getCabine(){
        return cabine;
    }

    public Pont getPont(){
        return pont;
    }
    
    /*création des setters*/
    
    public void setId(int id){
    	this.id = id;
    }
    
    public void setNom(String nom){
    	this.nom = nom;
    }
    
    public void setMarque(String marque){
        	this.marque = marque;
    }
      
    public void setCoque(Coque coque){
            this.coque = coque;
    }

    public void setCabine(Cabine cabine){
            this.cabine = cabine;
    }

    public void setPont(Pont pont){
            this.pont = pont;
    }
    
    /*création d'une méthode decrireBateau*/
    public void decrireBateau(){
 	   System.out.println("le bateau d'identifiant "+id+" a un nom "+nom+" et de marque "+marque);
    }



    public static void main(String[] args) {
        
		
        
        Bateau bateau1 = new Bateau(1, "Alice", "Simmer", null, null, null);
        Bateau bateau2 = new Bateau(2, "Alex", "Vadal", null, null, null);
        
        

        
        bateau1.decrireBateau();
        bateau2.decrireBateau();
    }   

}

