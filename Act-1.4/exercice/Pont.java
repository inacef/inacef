package exercice;
import exercice.Voile;

public class Pont {

      protected int idPont;
      private Double longueurPont;
      public String couleurPont;
      public Voile voile;
      
      /*constructeur par défaut*/ 
      
      public Pont(){
        
      }

      
      /*constructeur avec paramétres*/
      
      public Pont(int idPont, Double longueurPont, String couleurPont, Voile voile){
        this.idPont = idPont;
        this.longueurPont = longueurPont;
        this.couleurPont = couleurPont;
        this.voile = voile;
      }

      
      /*création des getters*/
      
      public int getIdPont(){
      return idPont;
      }
      
      public Double getLongueurPont(){
      return longueurPont;
      }
      
      public String getCouleurPont(){
      return couleurPont;
      }

      public Voile getVoile(){
      return voile;
      }
      
      /*création des setters*/
      
      public void setIdPont(int idPont){
        this.idPont = idPont;
      }
      
      public void setLongueurPont(Double longueurPont){
        this.longueurPont = longueurPont;
      }
      
      public void setCouleurPont(String couleurPont){
        this.couleurPont = couleurPont;
      }

      public void setVoile(Voile voile){
        this.voile = voile;
      }
  
      
      public void afficherCouleur(){
       System.out.println("le pont d'identifiant " +idPont+ " a une longueur "+longueurPont);
      }
      
     
      public String toString(){
		
		return "le pont est d'id "+this.idPont+" ,de longueur "+this.longueurPont+" , de couleur "+this.couleurPont+" et de voile "+this.voile;  
  }
  }


