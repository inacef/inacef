package exercice;
public class Coque {

		protected int idCoque;
		private Double longueurCoque;
		public String couleurCoque; 
			

	/*constructeur par défaut*/	
	    
	    public Coque(){
	    	
	    }
	    
	    /*constructeur avec paramétres*/
	    
	    public Coque(int idCoque, Double longueurCoque, String couleurCoque){
	    	this.idCoque = idCoque;
	    	this.longueurCoque = longueurCoque;
	    	this.couleurCoque = couleurCoque;
	    }
	    
	    /*création des getters*/
	    
	    public int getIdCoque(){
			return idCoque;
	    }
	    
	    public Double getLongueurCoque(){
			return longueurCoque;
	    }
	    
	    public String getCouleurCoque(){
			return couleurCoque;
	    }
	    
	    /*création des setters*/
	    
	    public void setIdCoque(int idCoque){
	    	this.idCoque = idCoque;
	    }
	    
	    public void setLongueurCoque(Double longueurCoque){
	    	this.longueurCoque = longueurCoque;
	    }
	    
	    public void setCouleurCoque(String couleurCoque){
	        	this.couleurCoque = couleurCoque;
	    }
	    
        /*création d'une méthode afficher*/
	    public void afficher(){
	 	   System.out.println("le coque du bateau d'identifiant "+idCoque+" a une longueur "+longueurCoque+" et une couleur est "+couleurCoque);
	    }

         public String toString(){
		
		return "Le coque est d'id" +this.idCoque+ ",de longueur" +this.longueurCoque+" et de couleur "+this.couleurCoque;
	    
	}
	}
	