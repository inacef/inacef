package exercice;
public class Voile {
	
	protected int idVoile;
	private Double largeurVoile;
	private Double longueurVoile;
	public String couleurVoile;
    private boolean enroule;
    private boolean deroule;
    
    /* constructeur par défaut */
    
    public Voile(){
    	
    }
  
    /*constructeur avec paramétres*/
    
    public Voile(int idVoile, Double largeurVoile, Double longueurVoile, String couleurVoile, boolean enroule, boolean deroule){
        	this.idVoile = idVoile;
        	this.largeurVoile = largeurVoile;
        	this.longueurVoile = longueurVoile;
        	this.couleurVoile = couleurVoile;
        	this.enroule = enroule;
        	this.deroule = deroule;
    }

	/*création des getters*/
    
    public int getId(){
		return idVoile;
    }
    
    public Double getLargeurVoile(){
    	return largeurVoile;
    }
    
    public Double getLongeurVoile(){
    	return longueurVoile;
    }
    
    public String getCouleurVoile(){
    	return couleurVoile;
    }
    
    public boolean getEnroule() {
		return enroule;
	}
    
    public boolean getDeroule() {
		return deroule;
	}
    
    /*création des setters*/
    
    public void setIdVoile(int idVoile){
    	this.idVoile = idVoile;
    }
    
    public void setLargeurVoile(Double largeurVoile){
    	this.largeurVoile = largeurVoile;
    }
    
    public void setLongeurVoile(Double longueur){
    	this.longueurVoile = longueurVoile;
    }
    
    public void setCouleurVoile(String couleurVoile){
    	this.couleurVoile = couleurVoile;
    }
    
    public void setEnroule(boolean enroule) {
		this.enroule = enroule;
	}
    
    public void setDeroule(boolean deroule) {
		this.deroule = deroule;
	}
    
    /*création de la méthode enrouler*/
    
    public boolean enrouler(){
		this.enroule = true;
		this.deroule = false;
    	return enroule;	
    }
    
    /*création de la méthode dérouler*/
    
    public boolean derouler(){
		this.deroule = true;
		this.enroule = false;
    	return deroule;
    	
    }


    public String toString(){
		
		return "la voile est d'id "+this.idVoile+" et de largeur "+this.largeurVoile+" de longueur "+this.longueurVoile+" de couleur "+this.couleurVoile+" enroule est "+this.enroule+ " deroule est "+this.deroule;

  

}
}