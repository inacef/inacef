package exercice;
public class Cabine {

	public int id;
	private Double longueur;
	private Double largeur; 
		

/*constructeur par défaut*/	
    
    public Cabine(){
    	
    }
    
    /*constructeur avec paramétres*/
    
    public Cabine(int id, Double longueur, Double largeur){
    	this.id = id;
    	this.longueur = longueur;
    	this.largeur = largeur;
    }
    
    /*création des getters*/
    
    public int getId(){
		return id;
    }
    
    public Double getLongueur(){
		return longueur;
    }
    
    public Double getLargeur(){
		return largeur;
    }
    
    /*création des setters*/
    
    public void setId(int id){
    	this.id = id;
    }
    
    public void setNom(Double longueur){
    	this.longueur = longueur;
    }
    
    public void setLargeur(Double largeur){
        	this.largeur = largeur;
    }
    

    
    public Double modifierLongueur(){
 	   return this.longueur *2;
    }



   public String toString(){
		
		return "la cabine est d'identifiant "+this.id+" ,delongueur "+this.longueur+" et de largeur "+this.largeur;
  
}
}