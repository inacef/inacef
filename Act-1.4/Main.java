import exercice.*;
/*
import Coque;
import Cabine;
import Pont;
import Voile;
*/

public class Main {

	public static void main(String[] args) {
		
		Coque coque1 = new Coque(2, 2.0, "Jaune");
		Cabine cabine1 = new Cabine(2, 2.5, 1.5);
		Voile voile1 = new Voile(2, 1.5, 2.5, "Bleue", false, true);
		Pont pont1 = new Pont(1, 2.0, "blanc", voile1);
		Bateau monbateau = new Bateau(1, "Alice", "Simmer", coque1, cabine1, pont1);


		
		System.out.println(monbateau);
	}
}