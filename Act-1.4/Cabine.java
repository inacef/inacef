﻿
public class Cabine {

	public int id;
	private Double longueur;
	private Double largeur; 
		

/*constructeur par défaut*/	
    
    public Cabine(){
    	
    }
    
    /*constructeur avec paramétres*/
    
    public Cabine(int id, Double longueur, Double largeur){
    	this.id = id;
    	this.longueur = longueur;
    	this.largeur = largeur;
    }
    
    /*création des getters*/
    
    public int getId(){
		return id;
    }
    
    public Double getLongueur(){
		return longueur;
    }
    
    public Double getLargeur(){
		return largeur;
    }
    
    /*création des setters*/
    
    public void setId(int id){
    	this.id = id;
    }
    
    public void setNom(Double longueur){
    	this.longueur = longueur;
    }
    
    public void setLargeur(Double largeur){
        	this.largeur = largeur;
    }
    

    /*création d'une méthode afficherLongueur*/
    public void afficherLongueur(){
 	   System.out.println("le coque d'identifiant "+id+" a une longueur "+longueur);
    }



    public static void main(String[] args) {
         
    
        
        Cabine cabine1 = new Cabine(2, 2.5, 1.5);

        Cabine cabine2 = new Cabine(2, 3.5, 2.5);


       cabine1.afficherLongueur();
       cabine2.afficherLongueur();
       
    }
  
}
