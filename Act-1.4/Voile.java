﻿public class Voile {
	
	protected int idVoile;
	private Double largeurVoile;
	private Double longueurVoile;
	public String couleurVoile;
    private boolean enroule;
    private boolean deroule;
    
    /* constructeur par défaut */
    
    public Voile(){
    	
    }
  
    /*constructeur avec paramétres*/
    
    public Voile(int idVoile, Double largeurVoile, Double longueurVoile, String couleurVoile, boolean enroule, boolean deroule){
        	this.idVoile = idVoile;
        	this.largeurVoile = largeurVoile;
        	this.longueurVoile = longueurVoile;
        	this.couleurVoile = couleurVoile;
        	this.enroule = enroule;
        	this.deroule = deroule;
    }

	/*création des getters*/
    
    public int getId(){
		return idVoile;
    }
    
    public Double getLargeurVoile(){
    	return largeurVoile;
    }
    
    public Double getLongeurVoile(){
    	return longueurVoile;
    }
    
    public String getCouleurVoile(){
    	return couleurVoile;
    }
    
    public boolean getEnroule() {
		return enroule;
	}
    
    public boolean getDeroule() {
		return deroule;
	}
    
    /*création des setters*/
    
    public void setIdVoile(int idVoile){
    	this.idVoile = idVoile;
    }
    
    public void setLargeurVoile(Double largeurVoile){
    	this.largeurVoile = largeurVoile;
    }
    
    public void setLongeurVoile(Double longueur){
    	this.longueurVoile = longueurVoile;
    }
    
    public void setCouleurVoile(String couleurVoile){
    	this.couleurVoile = couleurVoile;
    }
    
    public void setEnroule(boolean enroule) {
		this.enroule = enroule;
	}
    
    public void setDeroule(boolean deroule) {
		this.deroule = deroule;
	}
    
    /*création de la méthode enrouler*/
    
    public boolean enrouler(){
		this.enroule = true;
		this.deroule = false;
    	return enroule;	
    }
    
    /*création de la méthode dérouler*/
    
    public boolean derouler(){
		this.deroule = true;
		this.enroule = false;
    	return deroule;
    	
    }


    public static void main(String[] args) {
        Voile voile1 = new Voile();
        voile1.setIdVoile(1);
        voile1.setLargeurVoile(1.0);
        voile1.setLongeurVoile(2.0);
        voile1.setCouleurVoile("Rouge");
        voile1.setEnroule(true);
        voile1.setDeroule(false);
            
        Voile voile2 = new Voile(2, 1.5, 2.5, "Bleue", false, true);
        
        
        boolean Result1 = voile1.enrouler();
            System.out.println("le résultat de la méthode enrouler est: "+Result1);
             
        boolean Result2 = voile2.derouler();
            System.out.println("le résultat de la méthode derouler est: "+Result2);
            
        }

  

}