package bibliotheque;

import java.util.ArrayList;

public class Recette {

	public String nom;
	public String description;
	public int nvDifficulte;
	public ArrayList<String> astuces;
	public ArrayList<String> etapes;
	
	
	//constructeur par d�faut//
	public Recette(){
	}
	
	//constructeur avec param�tres//
	public Recette(String nom, String description, int nvDifficulte){
	    this.nom = nom;
		this.description = description;
		this.nvDifficulte = nvDifficulte;
		this.astuces = new ArrayList<String>();
		this.etapes = new ArrayList<String>();
	}
			
	//cr�ation des getters//
	public String getNom(){
		return(nom);
	}	
	
	public String getDescription(){
		return(description);
	}
	
	public int getNvDifficulte(){
		return(nvDifficulte);
	}
	
	public ArrayList<String> getAstuces(){
		return(astuces);
	}
	
	public ArrayList<String> getEtapes(){
		return(etapes);
	}
	
	
	//cr�ation des setters//
	public void setNom(String nom){
		this.nom = nom;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public void setNvDifficulte(int nvDifficulte){
		this.nvDifficulte = nvDifficulte;
	}
	
	public void addAstuce(String astuce){
		astuces.add(astuce);
	}
	
	public void addEtape(String etape){
		etapes.add(etape);
	}
	
	public String Afficher() {
		String recetteString = nom + "\n" + description + "\n" + nvDifficulte + "\nAstuces:\n";
		for (String astuce : astuces) {
			recetteString = recetteString + astuce + "\n";
		}
		recetteString = recetteString + "\nEtapes:\n";
		for (String etape : etapes) {
			recetteString = recetteString + etape + "\n";
		}
		
		return recetteString;
	}
		
}
