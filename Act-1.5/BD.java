package bibliotheque;

public class BD extends Livre {

	private boolean couleur;

	// constructeur par d�faut
	public BD() {
		super();
		this.couleur = true;
	}

	// constructeur avec param�tres
	public BD(String nom, String auteur, Double prix, int nbPage,
			boolean couleur) {
		super(nom, auteur, prix, nbPage);
		this.couleur = couleur;
	}

	// cr�ation de getter
	public boolean getCouleur() {
		return couleur;
	}

	// cr�ation de setter
	public void setCouleur(boolean couleur) {
		this.couleur = couleur;
	}

	public String Afficher() {
		String str1 = "";
		if (this.couleur = true) {
			str1 = "color�e";
		} else {
			str1 = "noir et blanc";
		}
		return (super.Afficher() + "la couleur de la bande dessin�e est " + str1);
	}
}
