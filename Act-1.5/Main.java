﻿package bibliotheque;

public class Main {

	public static void main(String[] args) {
		Livre l1 = new Livre("Le petit prince","St Exupéry",10.40, 50) ;
		Livre l2 = new Livre("Contes","Grimm",14.40,254) ;
		System.out.println(l1.Afficher()) ;
		System.out.println(l2.Afficher());

		BD b1 = new BD("Lucky Luke","Morris",10.40, 45, true);
		BD b2 = new BD("Tintin","Herge",200.40, 45, false) ;
		System.out.println(b1.Afficher()) ;
		System.out.println(b2.Afficher()) ;

		Manga m1 = new Manga("One piece","Eiichirō Oda",5.40, 62);
		Manga m2 = new Manga("Death Note","Tsugumi Ōba",7.40, 75) ;
		System.out.println(m1.Afficher()) ;
		System.out.println(m2.Afficher()) ;

		Roman r1 = new Roman("Dora","Dora", 3.5, 300) ;
		r1.setNbChapitre(12);
		r1.setDescription("Une description quelconque");

                LivreRecette lrc1 = new LivreRecette("Marmiton", "Philippe Etchebest", 15.98, 110);
                Recette rc1 = new Recette("Les pâtes crues", "Comment réaliser de délicieuses pâtes crues.", 3);
                rc1.addAstuce("Ne pas les faire cuire.");
                rc1.addAstuce("Attendre l'ebulition de l'eau");
                rc1.addEtape("Sortir les pâtes de leur emballage");
                lrc1.addRecette(rc1);
		System.out.println(lrc1.Afficher());
	}

}
