package bibliotheque;

public class Livre{
	
	protected String nom;
	protected String auteur;
	protected Double prix;
	protected int nbPage;
	
	//constructeur par d�faut//
	public Livre(){
	}
	
	//constructeur avec param�tres//
	public Livre(String nom, String auteur, Double prix, int nbPage){
	    this.nom = nom;
		this.auteur = auteur;
		this.prix = prix;
		this.nbPage = nbPage;
	}
			
	//cr�ation des getters//
	public String getNom(){
		return(nom);
	}	
	
	public String getAuteur(){
		return(auteur);
	}
	
	public Double getPrix(){
		return(prix);
	}
	
	public int nbPage(){
		return(nbPage);
	}	
	
	
	//cr�ation des setters//
	public void setNom(String nom){
		this.nom = nom;
	}
	
	public void setAuteur(String auteur){
		this.auteur = auteur;
	}
	
	public void stePrix(Double prix){
		this.prix = prix;
	}
	
	public void setNbPage(int nbPage){
		this.nbPage = nbPage;
	}
	
	//cr�ation d'une m�thode Afficher//
	public String Afficher(){
		String str ="Le nom du livre est "+this.nom+" et l'auteur est "+this.auteur+" et le prix est "+this.prix+" euros et le nombre de pages est "+this.nbPage;
	    return str;
	   
	}
	
	
}

