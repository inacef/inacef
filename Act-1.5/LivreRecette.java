package bibliotheque;

import java.util.ArrayList;

public class LivreRecette extends Livre{
	
	public ArrayList<Recette> recettes;

	// constructeur par d�faut

	public LivreRecette() {
		super();
		this.recettes = new ArrayList<Recette>();
	}

	// constructeur avec param�tres
	public LivreRecette(String nom, String auteur, Double prix, int nbPage) {
		super(nom, auteur, prix, nbPage);
		this.recettes = new ArrayList<Recette>();
	}
	
	public String Afficher() {
		String recetteString = super.Afficher() + "\n";
		for (Recette recette : recettes) {
			recetteString = recetteString + recette.Afficher();			
		}
		return recetteString;
	}
	
	public void addRecette(Recette recette) {
		this.recettes.add(recette);
	}
}
