package bibliotheque;

public class Roman extends Livre {

	private int nbChapitre;
	private String description;

	// constructeur par d�faut

	public Roman() {

		super();
	}

	// constructeur avec param�tres
	public Roman(String nom, String auteur, Double prix, int nbPage) {
		super(nom, auteur, prix, nbPage);
	}

	// cr�ation des getters
	public int getNbChapitre() {
		return nbChapitre;
	}

	public String getDescription() {
		return description;
	}

	// cr�ation des setters
	public void setNbChapitre(int nbChapitre) {
		this.nbChapitre = nbChapitre;
		System.out.println(nbChapitre);
	}

	public void setDescription(String description) {
		this.description = description;
		System.out.println(description);
	}

}
