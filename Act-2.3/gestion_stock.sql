-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 26 mars 2020 à 10:52
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `gestion_stock`
--

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id_c` int(11) NOT NULL AUTO_INCREMENT,
  `nom_c` varchar(255) NOT NULL,
  `tel_c` int(255) NOT NULL,
  `abonnement_c` enum('premium','vip','classique','') NOT NULL,
  `interet_c` enum('sport','cinema','musique','') NOT NULL,
  PRIMARY KEY (`id_c`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id_c`, `nom_c`, `tel_c`, `abonnement_c`, `interet_c`) VALUES
(1, 'mhamed', 20000001, 'premium', 'cinema'),
(2, 'haithem', 20000002, 'premium', 'sport'),
(3, 'najeh', 20000003, 'vip', 'musique'),
(4, 'emna', 20000004, 'vip', 'cinema'),
(5, 'ghassen', 20000005, 'vip', 'sport'),
(6, 'ines', 20000006, 'classique', 'sport');

-- --------------------------------------------------------

--
-- Structure de la table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
CREATE TABLE IF NOT EXISTS `fournisseur` (
  `id_f` int(11) NOT NULL AUTO_INCREMENT,
  `nom_f` varchar(255) NOT NULL,
  PRIMARY KEY (`id_f`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `fournisseur`
--

INSERT INTO `fournisseur` (`id_f`, `nom_f`) VALUES
(1, 'groupe a'),
(2, 'groupe b');

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `id_p` int(11) NOT NULL AUTO_INCREMENT,
  `nom_p` varchar(255) NOT NULL,
  `type_p` varchar(255) NOT NULL,
  `marque_p` varchar(255) NOT NULL,
  `ref_f` int(255) NOT NULL,
  PRIMARY KEY (`id_p`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`id_p`, `nom_p`, `type_p`, `marque_p`, `ref_f`) VALUES
(1, 'milkana', 'fromage', 'delice', 1),
(2, 'mamzouj', 'yaourt', 'delice', 1),
(3, 'delisso', 'lait', 'delice', 1),
(4, 'gaufrettes', 'biscuit', 'saida', 2),
(5, 'croustina', 'biscuit', 'saida', 2),
(6, 'momento', 'chocolat', 'saida', 2),
(7, 'vitup', 'yaourt', 'vitalait', 1),
(8, 'vitalio', 'jus', 'vitalait', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
