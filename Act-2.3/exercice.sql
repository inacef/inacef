
-- creation de la table user
CREATE TABLE user ( 
id_u INT PRIMARY KEY ,
firstname VARCHAR(255) NOT NULL ,
lastname VARCHAR(255) NOT NULL ,
email VARCHAR(255) NOT NULL); 


-- creation de la table offer
CREATE TABLE offer ( 
id_o INT PRIMARY KEY ,
offerName VARCHAR(255) NOT NULL ,
unitPriceTTC` FLOAT NOT NULL); 


-- creation de la table quotation
CREATE TABLE quotation ( 
id_q INT PRIMARY KEY,
description VARCHAR(255) NOT NULL,
turnover INT NOT NULL),
id_user INT NOT NULL; 


-- creation de la table quotatiocontent
CREATE TABLE quotationcontent ( 
id_qc INT PRIMARY KEY ,
id_quotation INT NOT NULL,
id_o INT NOT NULL,
quantity INT NOT NULL ,
totalTTC FLOAT NOT NULL); 


-- insertion des donnees dans la table user
INSERT INTO user (firstname, lastname, email)  
VALUES('mohamed', 'gharbi', 'm.garbi@gmail.com'),
      ('leila ', 'mrad', 'l.mrad@gmail.com'),
      ('anis', 'chouchen', 'a.chouchen@gmail.com'),
      ('salma', 'youssef', 's.youssef@gmail.com');

-- insertion des donnees dans la table offer
INSERT INTO offer (offerName, unitPriceTTC) 
VALUES('télévision', '858.25'), 
      ('pc portable', '1256.24'), 
      ('smartphone', '542.21'), 
      ('tablette', '356.24');

-- insertion des donnees dans la table quotation
INSERT INTO quotation (decription, turnover, id_user) 
VALUES('description numero1', 1222.22, 1), 
      ('description numero2 ', 1547.58 , 2), 
      ('description numero3', 154.98, 3), 
      ('description numero4', 5478.25, 2),
	  ('description numero5 ', 25796.3, 3),
	  ('description numero6 ', 2549.54 , 1),
	  ('description numero7', 5478.36, 2),
      ('description numero8 ', 6589.25, 4),
	  ('description numero9', 5478.25, 1),
	  ('description numero10', 548.25, 3),
	   

-- insertion des donnees dans la table quotatiocontent
INSERT INTO quotatiocontent (id_quotation, id_offer, quantity, totalTTC) 
VALUES (10, 4, 25, 1548.58),
       (5, 3, 10, 1045.25),
       (3, 1, 58, 8973.25),
	   (10, 2, 36, 2548.2), 
       (8, 2, 56, 36596.2), 
       (10, 4, 658, 154897.25),
       (10, 4, 54, 124458.2), 
       (5, 3, 25, 1456.2), 
       (6, 2, 54, 4587.3), 
       (4, 2, 35, 1456.1) 


-- ajout de la cle etrangere id_user dans la table quotation
ALTER TABLE quotation
ADD CONSTRAINT fk_user FOREIGN KEY (id_user) REFERENCES user(id_u);

-- ajout de la cle etrangere id_quotation dans la table quotationcontent
ALTER TABLE quotatiocontent
ADD CONSTRAINT fk_quotation FOREIGN KEY (id_quotation) REFERENCES quotation(id_qc);

-- ajout de la cle etrangere id_offer dans la table quotationcontent
ALTER TABLE quotatiocontent
ADD CONSTRAINT fk_offer FOREIGN KEY (id_offer) REFERENCES offer(id_o);


-- afficher le contenu du devis ayant l'id 10 (nom du produit, prix unitaire, quantité, total TTC)
SELECT o.offerName, o.unitPriceTTC, qc.quantity, qc.totalTTC FROM offer o INNER JOIN quotatiocontent qc on o.id_o=qc.id_offer WHERE qc.id_qc=10;

-- afficher la liste des devis créés par les utilisateurs ayant une adresse Email sous GMAIL.COM (id devis, nom et prénom de l'utilisateur, adresse email)
SELECT q.id_q, u.firstname, u.lastname, u.email FROM user u INNER JOIN quotation q ON u.id_u=q.id_user WHERE (email LIKE '%gmail.com%');

