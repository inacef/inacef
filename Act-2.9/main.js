$(document).ready(function(){
  $("#formulaire").submit(function(event)
  {
    results.innerHTML = "";
    var error = false;
    var warning = false;

    var date1 = new Date($('#dateNaissance').val());
    var date2 = Date.now();
    var diffDays = parseInt((date2 - date1) / (1000 * 60 * 60 * 24));
    var diffYears = diffDays/365;
    if (diffYears < 18) {
      error = true;
      results.innerHTML+=('<p class="error">Vous avez moins de 18 ans</p>');
    }

    var ml=document.formulaire.mail.value;
    if (ml==""){
      warning = true;
      results.innerHTML+=('<p class="warning">Entrez votre addresse mail</p>');
    }

    if (!error && !warning) {
      results.innerHTML+=('<p class="valid">Formulaire valide</p>');
    }

    event.preventDefault();
  });

});
