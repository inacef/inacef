package streamingMusique;

public class Main {
	public static void main(String[] args) {
		MusiqueImpl m1= new MusiqueImpl("Pour que tu m'aimes encore","Jean Jacques Goldman","Celine Dion","classique");
		MusiqueImpl m2= new MusiqueImpl("Parler � mon p�re","Jacques Veneruso","Celine Dion","classique");
		
		m1.afficher();
		m2.afficher();
		
		System.out.println(m1.verifier(m2));
		
		PlayListe p = new PlayListImpl("Coucou", "Classique");
		p.ajouter(m1);
		p.ajouter(m2);

		System.out.println(p);
	}
}