package streamingMusique;

public class MusiqueImpl implements Musique{

	protected String titre;
	protected String autheur;
	protected String interprete;
	protected String genre;
	
	//constructeur par d�faut
	public MusiqueImpl(){
		
	}
	
	//constructeur avec param�tres
	public MusiqueImpl(String titre, String autheur, String interprete, String genre){
		this.titre = titre;
		this.autheur = autheur;
		this.interprete = interprete;
		this.genre = genre;
	}
	
	//cr�ation des getters
	public String getTitre(){
		return titre;
	}
	public String getAutheur(){
		return autheur;
	}
	public String getInterprete(){
		return interprete;
	}
	public String getGendre(){
		return genre;
	}
	
	//cr�ation des setters
	public void setTitre(String titre){
		this.titre = titre;
	}
	public void setAuteur(String autheur){
		this.autheur = autheur;
	}
	public void setInterprete(String interprete){
		this.interprete = interprete;
	}
	public void setGenre(String genre){
		this.genre = genre;
	}
	
	//impl�mentation de la m�thode afficher
	public void afficher() {
		System.out.println("le titre de la musque est "+this.titre+" et son auteur est "+this.autheur+" et son interprete est "+this.interprete+" et son genre est "+this.genre);	
	}

	//impl�mentation de la m�thode verifier
	public boolean verifier(Musique m) {
		return this.equals(m);
	}
	
}
