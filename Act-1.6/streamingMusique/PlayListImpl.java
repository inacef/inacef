package streamingMusique;

import streamingMusique.MusiqueImpl;

public class PlayListImpl implements PlayListe {
	protected String nom;
	protected String genre;
	protected int nbTitres;
	protected final int MAX_MUSIQUES = 10;
	protected Musique[] tabMusique;

	// constructeur avec paramètres
	public PlayListImpl(String nom, String genre) {
		this.nom = nom;
		this.genre = genre;
		this.nbTitres = 0;
		this.tabMusique = new MusiqueImpl[MAX_MUSIQUES];
	}

	// creation des getters
	public String getNom() {
		return nom;
	}

	public String getGenre() {
		return genre;
	}

	public int getNbTitres() {
		return nbTitres;
	}

	public Musique[] getTabMusique() {
		return tabMusique;
	}

	// creation des setters
	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public void setNbTitres(int nbTitres) {
		this.nbTitres = nbTitres;
	}

	public Musique[] setTabMusique() {
		return tabMusique;
	}

	public String toString() {
		return "La playliste est de nom " + this.nom + " et de genre "
				+ this.genre + " et contient " + this.nbTitres + " titres.";
	}

	public void ajouter(Musique m) {
		if (nbTitres < MAX_MUSIQUES) {
			tabMusique[nbTitres] = m;
			nbTitres++;
		}
	}

}
