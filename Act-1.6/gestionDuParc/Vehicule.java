package gestionDuParc;

public abstract class Vehicule {

	private int annee;
	private int numMatricule;
	private int puissance;
	private Double prix;
	static int nbVehicule;

	// constructeur avec param�tres//
	public Vehicule(int annee, int numMatricule, int puissance, Double prix) {
		this.annee = annee;
		this.numMatricule = ++nbVehicule;
		this.puissance = puissance;
		this.prix = prix;
	}

	// cr�ation des getters//
	public int getAnnee() {
		return (annee);
	}

	public int getNumMatricule() {
		return (numMatricule);
	}

	public int puissance() {
		return (puissance);
	}

	public Double getPrix() {
		return (prix);
	}

	// cr�ation des setters//
	public void setAnnee(int annee) {
		this.annee = annee;
	}

	public void setNumMatricule(int numMatricule) {
		this.numMatricule = numMatricule;
	}

	public void setPuissance(int puissance) {
		this.puissance = puissance;
	}

	public void stePrix(Double prix) {
		this.prix = prix;
	}

	// methode toString
	public String toString() {
		String chaine = "";
		if (this.getClass().getName() == "Voiture") {
			chaine = "La voiture";
		} else {
			chaine = "Le camion";
		}

		return chaine + " de " + this.annee + " et matricule "
				+ this.numMatricule + " et d'une puissance de "
				+ this.puissance + "  CV et au prix de " + this.prix
				+ " euros a �t� enregistr�e";
	}

	// methode abstraire demarrer
	abstract void demarrer();

	// methode abstraite accelerer
	abstract void accelerer();

}
