package gestionDuParc;

public class Camion extends Voiture {

	// constructeru param�tr�
	public Camion(int annee, int numMatricule, int puissance, Double prix) {
		super(annee, numMatricule, puissance, prix);
	}

	public void demarrer() {
		System.out.println("Le camion de " + this.getAnnee() + " et matricule "
				+ this.getNumMatricule() + " d�marre!");
	}

	public void accelerer() {
		System.out.println("Le camion de " + this.getAnnee() + " et matricule "
				+ this.getNumMatricule() + " acc�lere!");
	}

}
