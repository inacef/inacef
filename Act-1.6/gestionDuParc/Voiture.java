package gestionDuParc;

public class Voiture extends Vehicule {

	// constructeru param�tr�
	public Voiture(int annee, int nbMatricule, int puissance, Double prix) {
		super(annee, nbMatricule, puissance, prix);
	}

	public void demarrer() {
		System.out.println("La voiture de " + this.getAnnee()
				+ " et matricule " + this.getNumMatricule() + " d�marre!");
	}

	public void accelerer() {
		System.out.println("La voiture de " + this.getAnnee()
				+ " et matricule " + this.getNumMatricule() + " acc�lere!");
	}

}
