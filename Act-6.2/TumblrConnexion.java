package tumblr;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TumblrConnexion {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:/Users/Lenovo/Downloads/chromedriver_win32/chromedriver.exe");
		WebDriver myDriver = new ChromeDriver();
		
		//Navigate to tumblr.com/login using javascript
		JavascriptExecutor js = (JavascriptExecutor)myDriver;
	
		//Launching the Site.		
        myDriver.get("https://www.tumblr.com/login");	
        
        WebElement Next =myDriver.findElement(By.id("signup_forms_submit"));
        		
        //enter email 		
        myDriver.findElement(By.id("signup_determine_email")).sendKeys("ines.nacef@talan.com");	
        
        //Click on Next button using JavascriptExecutor	
        js.executeScript("arguments[0].click();", Next);
        
        //Click on UsePassword button using JavascriptExecutor	
        WebElement UsePassword =myDriver.findElement(By.className("forgot_password_link"));
        Thread.sleep(3000);
        js.executeScript("arguments[0].click();", UsePassword);
        
        //enter password
        myDriver.findElement(By.id("signup_password")).sendKeys("20516509");		
        
        //Click on LogIn button using JavascriptExecutor	
        WebElement LogIn =myDriver.findElement(By.id("signup_forms_submit"));
        js.executeScript("arguments[0].click();", LogIn);
        		
       myDriver.close();		

	}

}

