package gestionDesExceptions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class MaximumWithExceptions {

	public static void main(String args[]) {

		BufferedReader br;

		try {
			br = new BufferedReader(new FileReader("Act-1.9/gestionDesExceptions/daaata.txt"));
			int max = -1;
			String line;

			try {
				line = br.readLine();
				while (line != null) {
					int n = Integer.parseInt(line);

					if (n > max) {
						max = n;
						line = br.readLine();
					}
				}
				System.out.println("Maximum = " + max);

			} catch (IOException e) {
				System.out.println("Erreur d'entree");
			}

		} catch (FileNotFoundException e) {
			System.out.println("Le fichier est introuvable.");
		}
	}
}
