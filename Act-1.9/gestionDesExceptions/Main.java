package gestionDesExceptions;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws Exception{
		Scanner Sc = new Scanner(System.in);
		int x;
		System.out.println("Saisir un nombre entre 10 et 30 inclus: ");
		x = Sc.nextInt();
		while ((x>= 10) || (x<=30)){
			System.out.println(x);
			throw new Exception("l'entier n'est pas entre 10 et 30");
		}
		Sc.close();

	}

}
