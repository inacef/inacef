package sauvegardeFichier;

import streamingMusique.User;

public class LireChargerSauvegarde {

	public static void main(String[] args) {
		User u = new User("Ines", "Nacef", "inacef");
		u.charger("Act-1.9/sauvegardeFichier/fichierMusiqueVirgule.txt", "FromDisk", "Rock");
		
		System.out.println(u);
		
		u.sauvegarder("Act-1.9/sauvegardeFichier/nouvellePlayliste.txt");

	}

}
