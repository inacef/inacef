package sauvegardeFichier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import streamingMusique18.MusiqueImpl;

public class LireFichier {

	public static void main(String[] args) {
		ArrayList<MusiqueImpl> musiques = new ArrayList<MusiqueImpl>();
		Path chemin = Paths.get("Act-1.9/sauvegardeFichier/fichierMusique.txt");
		InputStream input = null;
		try {input = Files.newInputStream(chemin);
		BufferedReader reader = new BufferedReader(new InputStreamReader(input));
		String s = null;
		while((s = reader.readLine()) != null){
		System.out.println(s);
		}
		input.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(musiques);
	}

}


