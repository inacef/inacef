package streamingMusique;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import streamingMusique.MusiqueImpl;
import streamingMusique.PlayListeImpl;

public class User {

	private String nomUser;
	private String prenomUser;
	private String pseudoUser;
	private ArrayList<PlayListeImpl> playlistes = new ArrayList<PlayListeImpl>();

	// constructeur avec param�tres
	public User(String nomUser, String prenomUser, String pseudoUser) {
		this.nomUser = nomUser;
		this.prenomUser = prenomUser;
		this.pseudoUser = pseudoUser;
		this.playlistes = new ArrayList<PlayListeImpl>();
	}

	// getters and setters
	public String getNomUser() {
		return nomUser;
	}

	public void setNomUser(String nomUser) {
		this.nomUser = nomUser;
	}

	public String getPrenomUser() {
		return prenomUser;
	}

	public void setPrenomUser(String prenomUser) {
		this.prenomUser = prenomUser;
	}

	public String getPseudoUser() {
		return pseudoUser;
	}

	public void setPseudoUser(String pseudoUser) {
		this.pseudoUser = pseudoUser;
	}

	public ArrayList<PlayListeImpl> getPlaylistes() {
		return playlistes;
	}

	public void setPlaylistes(ArrayList<PlayListeImpl> playlistes) {
		this.playlistes = playlistes;
	}

	// method toString
	public String toString() {
		String str = "L'utilisateur s'appelle " + this.prenomUser + " "
				+ this.nomUser + " . Son pseudo est: " + this.pseudoUser
				+ ".\n";
		for (PlayListeImpl playliste : playlistes) {
			str = str + playliste;
		}
		return str;
	}

	// method add playlist
	public void ajouterPlaylist(PlayListeImpl playliste) {
		this.playlistes.add(playliste);
	}

	// search in a playlist for duplicate music
	public boolean contains(String titre, String auteur) {
		for (PlayListeImpl playliste : playlistes) {
			for (MusiqueImpl musique : playliste.getListeMusique()) {
				if (musique.getTitre().equals(titre)
						&& musique.getAutheur().equals(auteur)) {
					return true;
				}
			}
		}
		return false;
	}

	// sort the music list in ascending order
	public void triAsc() {
		for (PlayListeImpl playliste : playlistes) {
			Collections.sort(playliste.getListeMusique());
		}
	}

	// sort the music list in descending order
	public void triDesc() {
		for (PlayListeImpl playliste : playlistes) {
			Collections.sort(playliste.getListeMusique(),
					Collections.reverseOrder());
		}
	}

	// method delete music by title
	public void remove(String titre) {
		for (PlayListeImpl playliste : playlistes) {
			playliste.remove(titre);
		}
	}

	//charger les musiques � partir d'un fichier
	public void charger(String fichier, String nomPlayliste, String genre) {
		Path path = Paths.get(fichier);
		try (Stream<String> stream = Files.lines(path)) {
			List<String> lignesFichier = new ArrayList<String>();
			lignesFichier = stream.collect(Collectors.toList());

			PlayListeImpl playlist = new PlayListeImpl(nomPlayliste, genre);
			for (String ligne : lignesFichier) {
				String[] part = ligne.split(",");
				MusiqueImpl musique = new MusiqueImpl(part[0], part[1],part[2], part[3]);
				playlist.add(musique);
			}
			ajouterPlaylist(playlist);
		} catch (IOException e) {
			System.out.println("Impossible de lire le fichier");
		}
	}

	//sauvegarder les musiques charg�es dans un autre fichier
	public void sauvegarder(String fichier) {
		for (PlayListeImpl playlist : playlistes) {
			try {
				Path path = Paths.get(fichier);
				byte[] donnees = playlist.toStringSauvegarde().getBytes();
				Files.write(path, donnees);
			} catch (IOException e) {
				System.out.println("Erreur lors de la sauvegarde");
			}
		}
	}

}
