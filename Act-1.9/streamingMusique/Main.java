package streamingMusique;

import streamingMusique.MusiqueImpl;
import streamingMusique.PlayListeImpl;
import streamingMusique.User;

public class Main {

	public static void main(String[] args) {

		MusiqueImpl musique1 = new MusiqueImpl("Pour que tu m'aimes encore",
				"Jean Jacques Goldman", "Celine Dion", "classique");
		MusiqueImpl musique2 = new MusiqueImpl("Parler � mon p�re",
				"Jacques Veneruso", "Celine Dion", "classique");
		MusiqueImpl musique3 = new MusiqueImpl("Parler � mon p�re",
				"Jacques Veneruso", "Celine Dion", "classique");
		MusiqueImpl musique4 = new MusiqueImpl("Hero", "Marque Taylor",
				"Enrique Iglesias", "Pop");
		MusiqueImpl musique5 = new MusiqueImpl("La solitudine",
				"Angelo Valsiglio", "Laura Pausini", "Classique)");

		System.out.println(musique1.toString() + "\n");
		System.out.println(musique2.toString() + "\n");
		System.out.println(musique3.toString() + "\n");
		System.out.println(musique4.toString() + "\n");
		System.out.println(musique5.toString() + "\n");

		//check if 2 musics are equal
		System.out.println(musique1.equals(musique2));

		PlayListeImpl playliste = new PlayListeImpl("Coucou", "Classique");

		playliste.add(musique1);
		playliste.add(musique5);
		playliste.add(musique2);
		playliste.add(musique3);

		System.out.println(playliste);

		PlayListeImpl playliste1 = new PlayListeImpl("Salut", "Pop");
		playliste1.add(musique4);
		playliste1.add(musique5);

		System.out.println(playliste1);

		playliste.removeDuplicate();
		System.out.println(playliste);

		User u = new User("Nacef", "Ines", "ines");
		u.ajouterPlaylist(playliste);
		u.ajouterPlaylist(playliste1);
		System.out.println(u);

		boolean exist = u.contains("Hero", "Marque Taylor");
		System.out.println(exist);

		System.out.println("### Trier par ordre ascendant");
		u.triAsc();

		System.out.println(u);

		System.out.println("### Trier par ordre descendant");
		u.triDesc();

		System.out.println(u);

		//remove music which its title is "la solitudine"
		u.remove("La solitudine");

		System.out.println(u);

	}
}
