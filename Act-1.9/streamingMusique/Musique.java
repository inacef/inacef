package streamingMusique;

import streamingMusique.MusiqueImpl;

public interface Musique {

	public String toString();

	public boolean equals(MusiqueImpl musique);

}
