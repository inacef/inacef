package streamingMusique;

import streamingMusique.Musique;
import streamingMusique.MusiqueImpl;

public class MusiqueImpl implements Musique, Comparable<MusiqueImpl> {

	private String titre;
	private String autheur;
	private String interprete;
	private String genre;

	// constructeur par d�faut
	public MusiqueImpl() {

	}

	// constructeur avec param�tres
	public MusiqueImpl(String titre, String autheur, String interprete,
			String genre) {
		this.titre = titre;
		this.autheur = autheur;
		this.interprete = interprete;
		this.genre = genre;
	}

	// getters
	public String getTitre() {
		return titre;
	}

	public String getAutheur() {
		return autheur;
	}

	public String getInterprete() {
		return interprete;
	}

	public String getGenre() {
		return genre;
	}

	// setters
	public void setTitre(String titre) {
		this.titre = titre;
	}

	public void setAutheur(String autheur) {
		this.autheur = autheur;
	}

	public void setIneterpret(String interprete) {
		this.interprete = interprete;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	// implementation of method toString
	public String toString() {
		return "La musique est de titre " + this.titre + ", autheur "
				+ this.autheur + ", interprete " + this.interprete
				+ " et genre " + this.genre;
	}
	
	public String toStringSauvegarde() {
		return titre + "," + autheur + "," + interprete + "," + genre + "\n";
	}

	// Implementation of method equals
	public boolean equals(MusiqueImpl musique) {
		if (musique.titre.equals(titre) && musique.autheur.equals(autheur)
				&& musique.interprete.equals(interprete)
				&& musique.genre.equals(genre)) {
			return true;
		} else {
			return false;
		}

	}


	// compare music by their titles
	@Override
	public int compareTo(MusiqueImpl other) {
		return this.titre.compareTo(other.getTitre());

	}

}
