package streamingMusique;

import streamingMusique.MusiqueImpl;

public interface PlayListe {

	public String toString();

	public void add(MusiqueImpl musique);

	public void removeDuplicate();

}
