package seleniumfirststep;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.lang.String;

public class TestCase1 {
	
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "C:/Users/Lenovo/Downloads/chromedriver_win32/chromedriver.exe");
		WebDriver myDriver = new ChromeDriver();
		
		String baseUrl = "http://demo.guru99.com/test/newtours/";
        String attenduTitle = "Welcome: Mercury Tours";
        String actualTitle = "";
        
        myDriver.get(baseUrl);
        String currentUrl = myDriver.getCurrentUrl();
        System.out.println(currentUrl);
        
        if (currentUrl.contentEquals (baseUrl)) {
            System.out.println ("Meme Url! Test r�ussi!");
        } else {
            System.out.println ("Autre Url! �chec du test");
        }
        
        actualTitle = myDriver.getTitle ();
        
        if (actualTitle.contentEquals (attenduTitle)) {
            System.out.println ("Meme titre! Test r�ussi!");
        } else {
            System.out.println ("Autre titre! �chec du test");
        }
        
        String pageSource = myDriver.getPageSource();
        System.out.println(pageSource);
        
        myDriver.close ();
	}
 
}
