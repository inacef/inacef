$(document).ready(function () {
$.ajax({
    type: "GET",
    url: "MonEvenement.xml",
    dataType: "xml",
    success: xpathParser
   });
});
function xpathParser(xml) {
	//récupérer date de l'événement
	dateEvenement = xml.evaluate(".//evenement/date/text()", xml, null, XPathResult.STRING_TYPE, null).stringValue;
	//récupérer budget et devise de l'événement
	budgetEvenement = xml.evaluate(".//evenement/budget/text()", xml, null, XPathResult.NUMBER_TYPE, null).numberValue;
	deviseBudgetEvenement = xml.evaluate(".//evenement/budget/@devise", xml, null, XPathResult.STRING_TYPE, null).stringValue;
	//récupérer le nom du deuxieme invité
	nomDeuxiemeInvite = xml.evaluate(".//evenement/participants/participant[@type='invites'][2]/nom/text()", xml, null, XPathResult.STRING_TYPE, null).stringValue;
	//affichage des résultats récupérés
	$('body').append("<p>" + "la date de l'evenement est: " + dateEvenement + "<br/>" + "le budget de l'evenement est: " + budgetEvenement + " " + deviseBudgetEvenement + "<br/>" + "le nom du deuxieme invite est: " + nomDeuxiemeInvite + "</p>");
}
