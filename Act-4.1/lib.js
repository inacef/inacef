function afficher(){
// On crée l'élément conteneur 1
var mainDiv1 = document.createElement('div');
mainDiv1.id = 'part1';

// On crée tous les nœuds textuels, pour plus de facilité
var textNodes = [

    document.createTextNode('Talan Academy '),
    document.createTextNode('est une école de reconversion interne à '),
    document.createTextNode('Talan '),
    document.createTextNode(', qui propose une formation professionnalisante basée sur une pédagogie innovante.')
];

// On crée les deux <strong> et les deux <a>
var taStrong = document.createElement('strong');
taStrong.appendChild(textNodes[0]);

var orgLink = document.createElement('a');
orgLink.href ='https://talan.com';
orgLink.appendChild(textNodes[2]);

// On insère le tout dans mainDiv
mainDiv1.appendChild(taStrong);
mainDiv1.appendChild(textNodes[1]);
mainDiv1.appendChild(orgLink);
mainDiv1.appendChild(textNodes[3]);

// On insère mainDiv dans le <body>
document.body.appendChild(mainDiv1);

// On crée l'élément conteneur 2
var mainDiv2 = document.createElement('div');
mainDiv2.id = 'part2';

// On crée tous les nœuds textuels, pour plus de facilité
var cursus = [
    document.createTextNode('Java'),
    document.createTextNode('PHP'),
    document.createTextNode('Test et validation')
];

// On crée le paragraphe
var paragraph = document.createElement('p');
var paragraphText = document.createTextNode('Les cursus proposés :');
paragraph.appendChild(paragraphText);


// On crée la liste, et on boucle pour ajouter chaque item
var uList = document.createElement('ul'),
    uItem;

for (var i = 0, c = cursus.length; i < c; i++) {
    uItem = document.createElement('li');

    uItem.appendChild(cursus[i]);
    uList.appendChild(uItem);
}

// On insère le tout dans mainDiv
mainDiv2.appendChild(paragraph);
mainDiv2.appendChild(uList);

// On insère mainDiv dans le <body>
document.body.appendChild(mainDiv2);
}
