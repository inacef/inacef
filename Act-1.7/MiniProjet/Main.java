package miniprojet;

import miniprojet.Predateur.categorieAge;
import miniprojet.Predateur.groupe;
import miniprojet.Predateur.sexe;

public class Main {

	public static void main(String[] args) {
		Lion l = new Lion(sexe.MALE,categorieAge.JEUNE,"fort",groupe.GROUPE,true,"ALPHA","tr�s imp�tieux");
		
		System.out.println(l.seNourrir());
		System.out.println(l.chasser());
	    System.out.println(l.courir());
		System.out.println(l.seReproduire());
		System.out.println(l.emettreSon());
		System.out.println(l.toString());
		System.out.println(l.entendreSon());
	    System.out.println(l.seSeparer());
	    
		Ours O=new Ours(sexe.MALE,categorieAge.JEUNE,"faible",groupe.GROUPE,"tr�s aggressif","impuissznt",true);
		
		System.out.println(O.seNourrir());
		System.out.println(O.chasser());
	    System.out.println(O.courir());
		System.out.println(O.seReproduire());
		System.out.println(O.emettreSon());
		System.out.println(O.toString());
		System.out.println(O.gripmer());
		System.out.println(O.creuser());
		System.out.println(O.hiverner());

	}


}
