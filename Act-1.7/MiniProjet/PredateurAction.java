package miniprojet;

public interface PredateurAction {
	public String seNourrir();
	public String chasser();
	public String courir();
	public String seReproduire();
	public String emettreSon();
}
