package miniprojet;

public class Ours extends Predateur implements PredateurAction, OursAction{
	
	public String factAgressivite;
	public String puissance;
	public boolean statHivernation;
	
	//constructeur
	public Ours(sexe sexe, categorieAge categorieAge, String force, groupe groupe, String factAgressivite, String puissance, boolean statHivernation) {
		super(sexe, categorieAge, force, groupe);
		this.factAgressivite = factAgressivite;
		this.puissance = puissance;
		this.statHivernation = statHivernation;
	}

	//getters
	public String getFactAgressivite() {
		return factAgressivite;
	}

	public String getPuissance() {
		return puissance;
	}

	public boolean getStatHivernation() {
		return statHivernation;
	}
	
	public void setFactAgressivite(String factAgressivite) {
		this.factAgressivite = factAgressivite;
	}
	
	public void setPuissance(String puissance) {
		this.puissance = puissance;
	}

	public void setStatHivernation(boolean statHivernation) {
		this.statHivernation = statHivernation;
	}

	//implémentation des méthodes
	public String seNourrir(){
		return "Je suis un ours, je suis omnivore";
	}
			
	public String chasser(){
		groupe gp = groupe.GROUPE;
		if(gp == groupe.GROUPE){
			return "Je ne chasses pas";
		}
		else{
			return "Je chasses";
		}
	}
	
	public String courir(){
		categorieAge ctag = categorieAge.JEUNE;
		if(ctag == categorieAge.JEUNE){
			return "Je cours vite";
		}
		else if(ctag == categorieAge.ADULTE){
			return "Je cours moyennement";
		}
		else{
			return "Je cours lentement";
		}
	}
	
	public String seReproduire(){
		return "Je suis un ours, je me reproduits";
	}
	
	public String emettreSon(){
		groupe gp = groupe.GROUPE;
		if(gp == groupe.GROUPE){
			return "J'hurle pour dissuader d'autres prédateurs";
		}
		else{
			return "Je n'hurle pas";
		}
		}
	
	public String toString(){
		
		return this.getClass().getSimpleName()+" de sexe "+super.getsexe()+" et de catégorie d'age "+super.getCategorieAge()+ " et " +super.getForce()+ " est en "+super.getGroupe()+" et "+this.factAgressivite+" et "+this.puissance+" et "+this.statHivernation;
		
	}
	public String gripmer() {
		
		return "Je grimpe";
	}

	public String creuser(){
		return "Je creuses";
	}
	
	public String hiverner(){
		if (this.statHivernation = true){
			return "hiverne";
		}
		else
		{
			return "hiverne pas";
		}
	}


}
