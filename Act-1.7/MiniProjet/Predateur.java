package miniprojet;

public abstract class Predateur implements PredateurAction{
	
	enum sexe{
		MALE, FEMELLE
	}
	
	enum categorieAge{
		JEUNE, ADULTE, VIEUX
	}
	
	enum groupe{
		GROUPE, SOLIDAIRE
	}
	
	private sexe sexe;
	private categorieAge categorieAge;
	private String force;
	private groupe groupe;
	
	//constructeur paramétré
	public Predateur(sexe sexe, categorieAge categorieAge, String force, groupe groupe) {
		this.sexe = sexe;
		this.categorieAge = categorieAge;
		this.force = force;
		this.groupe = groupe;
	}

	// getters
	public sexe getsexe() {
		return (sexe);
	}

	public categorieAge getCategorieAge() {
		return (categorieAge);
	}

	public String getForce() {
		return (force);
	}

	public groupe getGroupe() {
		return (groupe);
	}

	//setters
	public void setSexe(sexe sexe) {
		this.sexe = sexe;
	}

	public void setCategorieAge(categorieAge categorieAge) {
		this.categorieAge = categorieAge;
	}

	public void setForce(String force) {
		this.force = force;
	}

	public void setGroupe(groupe groupe) {
		this.groupe = groupe;
	}
	
	public abstract String toString();
		
}
