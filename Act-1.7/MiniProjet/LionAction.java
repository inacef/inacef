package miniprojet;

public interface LionAction {
	public String toString();
	public String entendreSon();
	public String seSeparer();
}
