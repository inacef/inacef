package miniprojet;

import java.util.Set;

@SuppressWarnings("unused")
public class Lion extends Predateur implements PredateurAction, LionAction{
	
	private boolean factDomination;
	private String rangDomination;
	private String factImpetuosite;
	
	public Lion(sexe sexe, categorieAge categorieAge, String force, groupe groupe, boolean factDomination, String rangDomination, String factImpetuosite) {
		super(sexe, categorieAge, force, groupe);
		this.factDomination = factDomination;
		this.rangDomination = rangDomination;
		this.factImpetuosite = factImpetuosite;
	}

	//getters
	public boolean getFactDomination() {
		return factDomination;
	}

	public String getRangDomination() {
		if (this.factDomination == true) {
			return this.rangDomination;
		} 
		else {
			return "Le lion n'est pas dominant ! ";
		}
	}

	public String getFactImeptuosite() {
		return factImpetuosite;
	}
	
	//setters
	public void setFactDomination(boolean factDomination) {
		this.factDomination = factDomination;
	}
	public void setRangDomination(String rangDomination) {
		this.rangDomination = rangDomination;
	}

	public void setFactImeptuosite(String factImetuosite) {
		this.factImpetuosite = factImetuosite;
	}
	
	//implémentation des methodes
	public String seNourrir(){
		return "Je suis un lion, je me nourris uniquement de la viande";
	}
			
	public String chasser(){
		groupe gp = groupe.GROUPE;
		if(gp == groupe.GROUPE){
		return "Je chasses";
		}
		else{
			return "Je ne chasses pas";
		}
	}
	
	public String courir(){
		return "Je suis un lion, je cours";
	}
	
	public String seReproduire(){
		return "Je suis un lion, je me reproduits";
	}
	
	public String emettreSon(){
		groupe gp = groupe.GROUPE;
		if(gp == groupe.GROUPE){
			return "Je rugis pour communiquer avec mes semblables";
		}
		else{
			return "Je ne rugis pas";
		}
	}
	
	public String toString(){
		
		return this.getClass().getSimpleName()+" de sexe "+super.getsexe()+" et de catégorie d'age "+super.getCategorieAge()+ " et " +super.getForce()+ " et son rang est "+this.rangDomination+" est en "+super.getGroupe()+" et "+this.factImpetuosite;
		
	}

	public String entendreSon() {
		boolean dors = false;
		boolean malade = false;
		if ((dors == false) || (malade == false)){
			return "J'entends un son";
		}
		else{
			return "Je n'entends pas de son";
		}
	}

	public String seSeparer() {
		groupe gp = groupe.GROUPE;
		if(gp == groupe.GROUPE){
			return "Je me sépares temporairement";
		}
		else{
			return "Je ne me sépares pas";
		}
	}
	

}
