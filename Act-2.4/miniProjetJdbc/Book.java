package miniProjetJdbc;

public class Book {

	private int id;
	private String title;
	private String author;
	private String editor;
	private int pageNb;
	private String summary;
	private int library;

	public Book() {
	}
	
	// constructeur avec param�tres
	public Book(String title, String author, String editor,
			int pageNb, String summary, int library) {
		this.title = title;
		this.author = author;
		this.editor = editor;
		this.pageNb = pageNb;
		this.summary = summary;
		this.library = library;
	}

	// getters and setters

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public int getPageNb() {
		return pageNb;
	}

	public void setPageNb(int pageNb) {
		this.pageNb = pageNb;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getLibrary() {
		return library;
	}

	public void setLibrary(int library) {
		this.library = library;
	}

	// methode pour afficher le livre
	public String toString() {
		return "livre d'id " + this.id + ", title=" + this.title
				+ ", author=" + this.author + ", editor=" + this.editor
				+ ", pageNb=" + this.pageNb + ", summary=" + this.summary
				+ ", library=" + this.library;
	}

	// methode pour v�rifier si deux instances de book sont �gales
	public boolean equals(Book obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (editor == null) {
			if (other.editor != null)
				return false;
		} else if (!editor.equals(other.editor))
			return false;
		if (id != other.id) {
			return false;
		}
		if (library == other.library)
			return false;
		if (pageNb != other.pageNb)
			return false;
		if (summary == null) {
			if (other.summary != null)
				return false;
		} else if (!summary.equals(other.summary))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

}
