package miniProjetJdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


public class Main {

	private static String driverName = "com.mysql.jdbc.Driver";
	private static String url = "jdbc:mysql://localhost:3306/gestionbibliotheque";
	private static String user = "root";
	private static String password = "";

	public static void main(String[] args) {
		
		try {
			Class.forName(driverName);
			Connection con = DriverManager.getConnection(url, user, password);

			LibraryManager manager = new LibraryManager(con);
			
			//inserer les donnees de deux librarires
			Library library = new Library("Bibliotheque Municipale", "102 Avenue de la Liberte", "71500120");
			library = manager.createLibrary(library);

			Library library_universite = new Library("Biblio Universite", "El Manar 2", "71122133");
			library_universite = manager.createLibrary(library_universite);
			
			//inserer les donnes de deux livres
			Book book1 = new Book("Adultere", "Paulo Coelho", "Flammarion", 150, "Roman paru en 2014", library.getId());
			Book book2 = new Book("Data Science", "Dickens", "Eyrolles", 230, "Formation en Data Science", library_universite.getId());
			manager.createBook(book1);
			manager.createBook(book2);
			
			//ajouter un livre a la librairie 1		
			Book book3 = new Book("Apprendre a aimer", "Nedra Gharyani", "Al Kiteb", 510, "Developpement personnel", library.getId());
			manager.createBook(book3);
			
			//inserer les donnees de deux utilisateurs
			User user1 = new User ("Ines", "Nacef", "06 Cite Riadh", "25632563", "i.nacef@yahoo.fr");
			User user2 = new User ("Mouna", "Makni", "08 Avenue Essalem", "24854756", "m.makni@yahoo.fr");
			manager.createUser(user1);
			manager.createUser(user2);
			
			//inserer les donnees de deux prets
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Rent rent1 = new Rent(1, 1, 
					dateFormat.parse("2020-03-25"), 
					dateFormat.parse("2020-03-30"));
			Rent rent2 = new Rent (3, 2, 
					dateFormat.parse("2020-03-23"), 
					dateFormat.parse("2020-03-29"));
			manager.createRent(rent1);
			manager.createRent(rent2);
			Rent rent3 = new Rent(2, 2, 
					dateFormat.parse("2020-03-29"), 
					dateFormat.parse("2020-04-02"));
			manager.createRent(rent3);
			
			// Afficher les livres d'une librairie 
			System.out.println("## Affichage des livre de la librairie 1");
			for (Book book : manager.getBooksByLibrary(1)) {
				System.out.println(book);
			}
			
			// Rechercher un livre par son Id
			System.out.println("## Rechercher le livre d'id 3");
			Book book = manager.getBookById(3);
			    System.out.println(book);
			
			// Rhercher un livre par son auteur
			System.out.println("## Afficher les livres de l'auteur Stephen King");
			Book book4 = manager.getBookByAuthor("Stephen King");
			    System.out.println(book4);
			
			// Rhercher un utilisateur par son Id
			System.out.println("## Rechercher l'utilisateur d'id 1");
			User user = manager.getUserById(1);
		    System.out.println(user);
		    
		    // trier les prets par date de fin decroissante d'une librairie
		    System.out.println("## Trier les prets d'une librairie par date de fin décroissante");
		    for (Rent rent : manager.getRentsByLibrary(2)) {
		  	System.out.println(rent);
		    }
		    
		    // Affihcer les livres en cours de pret d'une librairie
		    System.out.println("## Affichage des livres en cours de pret de la librairie 2");
		    for (Book book5 : manager.getRentedBooksByLibrary(2)) {
		    System.out.println(book5);
		    }
		    
		    // Afficher les livres en cours de pret d'un utilisateur
		    System.out.println("## Affichage des livres en cours de pret de l'utilisateur 2");
		    for (Book book6 : manager.getRentedBooksByUser(2)) {
		    System.out.println(book6);
		    }

			con.close();
		} catch (Exception e) {
			System.out.println("Connection a la BD impossible.");
			e.printStackTrace();
		}

	}

}
