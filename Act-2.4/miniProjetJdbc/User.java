package miniProjetJdbc;

import java.util.ArrayList;

public class User {
	private int id;
	private String prenom;
	private String nom;
	private String address;
	private String numTel;
	private String email;
	private ArrayList<Rent> rents;

	public User(){
		
	}
	
	// constructeur avec param�tres
	public User(String prenom, String nom, String address,String numTel, String email) {
		this.prenom = prenom;
		this.nom = nom;
		this.address = address;
		this.numTel = numTel;
		this.email = email;
	}

	// getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ArrayList<Rent> getRents() {
		return rents;
	}

	public void setRents(ArrayList<Rent> rents) {
		this.rents = rents;
	}

	// methode pour afficher l'utilisateur
	public String toString() {
		return "l'utilisateur d'id " + this.id + ", prenom="
				+ this.prenom + ", nom=" + this.nom + ", address="
				+ this.address + ", numTel=" + this.numTel + ", email="
				+ this.email + ", rents=" + this.rents;
	}

	// methode pour v�rifier si deux instances de User sont �gales
	public boolean equals(User obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (id != other.id)
			return false;

		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (numTel != other.numTel)
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}

}
