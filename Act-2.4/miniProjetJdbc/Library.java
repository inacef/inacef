package miniProjetJdbc;

import java.util.ArrayList;

public class Library {
	private int id;
	private String name;
	private String address;
	private String numTel;
	private ArrayList<Book> books;
	private ArrayList<Rent> rents;
	
	//constructeur avec paramètres
	public Library(String name, String address, String numTel){
		this.name = name;
		this.address = address;
		this.numTel = numTel;
		this.books = new ArrayList<Book>();
		this.rents = new ArrayList<Rent>();
	}

	
	//getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getNumTel() {
		return numTel;
	}

	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}

	public ArrayList<Book> getBooks() {
		return books;
	}

	public void setBooks(ArrayList<Book> books) {
		this.books = books;
	}

	public ArrayList<Rent> getRents() {
		return rents;
	}

	public void setRents(ArrayList<Rent> rents) {
		this.rents = rents;
	}

}
