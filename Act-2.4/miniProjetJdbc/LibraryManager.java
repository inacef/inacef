package miniProjetJdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LibraryManager {
	private Connection con;
	
	public LibraryManager(Connection con) {
		this.con = con;
	}
	
	public Library createLibrary(Library library) {
		try {
			String query = "INSERT INTO library (name, address, num_tel) VALUES (?, ? , ?)";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, library.getName());
			stmt.setString(2, library.getAddress());
			stmt.setString(3, library.getNumTel());
			stmt.executeUpdate();
			
			ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                library.setId(generatedKeys.getInt(1));
            }
			
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors de la creation d'un enregistrement Library.");
			e.printStackTrace();
		}	
		return library;
	}

	public void createBook(Book book) {
		try {
			String query = "INSERT INTO book (title, author, editor, page_nb, summary, library_id) VALUES (?, ? , ?, ?, ?, ?)";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, book.getTitle());
			stmt.setString(2, book.getAuthor());
			stmt.setString(3, book.getEditor());
			stmt.setInt(4,  book.getPageNb());
			stmt.setString(5, book.getSummary());
			stmt.setInt(6, book.getLibrary());
			stmt.execute();
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors de la creation d'un enregistrement Book.");
			e.printStackTrace();
		}		
	}
	
	public void createUser(User user) {
		try {
			String query = "INSERT INTO user (prenom, nom, address, num_tel, email) VALUES (?, ? , ?, ?, ?)";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, user.getPrenom());
			stmt.setString(2, user.getNom());
			stmt.setString(3, user.getAddress());
			stmt.setString(4,  user.getNumTel());
			stmt.setString(5, user.getEmail());
			stmt.execute();
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors de la creation d'un enregistrement User.");
			e.printStackTrace();
		}		
	}
	
	public void createRent(Rent rent) {
		try {
			String query = "INSERT INTO rent (book_id, user_id, date_pret, date_fin) VALUES (?, ? , ?, ?)";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, rent.getBook());
			stmt.setInt(2, rent.getUser());
			stmt.setDate(3, new Date(rent.getDatePret().getTime()));
			stmt.setDate(4, new Date(rent.getDateFin().getTime()));
			stmt.execute();
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors de la creation d'un enregistrement Rent.");
			e.printStackTrace();
		}		
	}

	public List<Book> getBooksByLibrary(int libraryId) {
		List<Book> books = new ArrayList<Book>();
		try {
			String query = "SELECT * FROM book WHERE library_id = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, libraryId);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()){
				
				Book book = new Book();
				book.setId(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setEditor(rs.getString(4));
				book.setPageNb(rs.getInt(5));
				book.setSummary(rs.getString(6));
				book.setLibrary(rs.getInt(7));
				
				books.add(book);
			}
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors de l'affichage des livres de la bibliotheque.");
			e.printStackTrace();
		}		
		return books;
	}

	public Book getBookById(int id) {
		try {
			String query = "SELECT * FROM Book WHERE id = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			
			if (rs.next()){
				
				Book book = new Book();
				book.setId(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setEditor(rs.getString(4));
				book.setPageNb(rs.getInt(5));
				book.setSummary(rs.getString(6));
				book.setLibrary(rs.getInt(7));
				
				return book;
			}
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors de l'affichage du livre par son id de la bibliotheque.");
			e.printStackTrace();
		}		

		return null;
	}

	public Book getBookByAuthor(String author) {
		try {
			String query = "SELECT * FROM Book WHERE author = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setString(1, author);
			ResultSet rs = stmt.executeQuery();
			
			if (rs.next()){
				
				Book book = new Book();
				book.setId(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setEditor(rs.getString(4));
				book.setPageNb(rs.getInt(5));
				book.setSummary(rs.getString(6));
				book.setLibrary(rs.getInt(7));
				
				return book;
			}
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors de l'affichage du livre par son auteur de la bibliotheque.");
			e.printStackTrace();
		}		

		return null;
	}

	
	public User getUserById(int id) {
		try {
			String query = "SELECT * FROM User WHERE id = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			
			if (rs.next()){
				
				User user = new User();
				user.setId(rs.getInt(1));
				user.setPrenom(rs.getString(2));
				user.setNom(rs.getString(3));
				user.setAddress(rs.getString(4));
				user.setNumTel(rs.getString(5));
				user.setEmail(rs.getString(6));
				
				return user;
			}
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors de l'affichage d'un utilisateur par son id.");
			e.printStackTrace();
		}		

		return null;
	}
	
	public List<Rent> getRentsByLibrary(int libraryId) {
		List<Rent> rents = new ArrayList<Rent>();
		try {
			String query = "SELECT * FROM rent INNER JOIN book ON rent.book_id = book.id "
					+ "INNER JOIN library ON book.library_id = library.id "
					+ "WHERE library.id = ? ORDER BY date_fin DESC";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, libraryId);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()){
				
				Rent rent = new Rent();
				rent.setId(rs.getInt(1));
				rent.setBook(rs.getInt(2));
				rent.setUser(rs.getInt(3));
				rent.setDatePret(rs.getDate(4));
				rent.setDateFin(rs.getDate(5));
				
				
				rents.add(rent);
			}
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors du tri des prets de la bibliotheque par date de fin decroissante.");
			e.printStackTrace();
		}		
		return rents;
	}
	
	public List<Book> getRentedBooksByLibrary(int libraryId) {
		List<Book> books = new ArrayList<Book>();
		try {
			String query = "SELECT book.*, rent.date_fin FROM book "
					+ "INNER JOIN rent ON book.id = rent.book_id "
					+ "WHERE book.library_id = ? AND CURRENT_DATE()BETWEEN rent.date_pret AND rent.date_fin";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, libraryId);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()){
				
				Book book = new Book();
				book.setId(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setEditor(rs.getString(4));
				book.setPageNb(rs.getInt(5));
				book.setSummary(rs.getString(6));
				book.setLibrary(rs.getInt(7));
				
				books.add(book);
			}
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors de l'affichage des livres de la bibliotheque.");
			e.printStackTrace();
		}		
		return books;
	}
	
	public List<Book> getRentedBooksByUser(int libraryUser) {
		List<Book> books = new ArrayList<Book>();
		try {
			String query = "SELECT book.*, rent.user_id FROM book "
					+ "INNER JOIN rent ON book.id = rent.book_id "
					+ "WHERE book.library_id = ?";
			PreparedStatement stmt = con.prepareStatement(query);
			stmt.setInt(1, libraryUser);
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()){
				Book book = new Book();
				book.setId(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setAuthor(rs.getString(3));
				book.setEditor(rs.getString(4));
				book.setPageNb(rs.getInt(5));
				book.setSummary(rs.getString(6));
				book.setLibrary(rs.getInt(7));
				
				books.add(book);
			}
		} catch (SQLException e) {
			System.out.println("Une erreur est survenue lors de l'affichage des livres de la bibliotheque.");
			e.printStackTrace();
		}		
		return books;
	}
	
}
