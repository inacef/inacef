package miniProjetJdbc;

import java.util.Date;

public class Rent {
	private int id;
	private int user;
	private int book;
	private Date datePret;
	private Date dateFin;
	
	public Rent(){
	}
	
	// constructeurs avec param�tres
	public Rent(int book, int user, Date datePret, Date dateFin){
		this.user = user;
		this.book = book;
		this.datePret = datePret;
		this.dateFin = dateFin;
	}
	
	//getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int libraryUser) {
		this.user = libraryUser;
	}

	public int getBook() {
		return book;
	}

	public void setBook(int book) {
		this.book = book;
	}

	public Date getDatePret() {
		return datePret;
	}

	public void setDatePret(Date datePret) {
		this.datePret = datePret;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	
	//methode pour afficher le pret
	public String toString() {
		return "le pret d'id "+ this.id + ", user=" + this.user + ", book=" + this.book
				+ ", datePret=" + this.datePret + ", dateFin=" + this.dateFin;
	}
	
	//methode pour v�rifier si deux instances de Pret sont �gales
	public boolean equals (Rent obj){
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rent other = (Rent) obj;
		if (book == other.book)
			return false;
		if (dateFin == null) {
			if (other.dateFin != null)
				return false;
		} else if (!dateFin.equals(other.dateFin))
			return false;
		if (datePret == null) {
			if (other.datePret != null)
				return false;
		} else if (!datePret.equals(other.datePret))
			return false;
		if (id != other.id)
			return false;
		if (user == other.user)
			return false;
		return true;

	}
}
